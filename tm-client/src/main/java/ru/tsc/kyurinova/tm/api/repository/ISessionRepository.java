package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

public interface ISessionRepository {
    @Nullable SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);
}
