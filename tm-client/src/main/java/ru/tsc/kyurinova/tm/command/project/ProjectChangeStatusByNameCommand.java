package ru.tsc.kyurinova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.endpoint.Status;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change status by name...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        serviceLocator.getProjectEndpoint().changeStatusByNameProject(session, name, status);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
