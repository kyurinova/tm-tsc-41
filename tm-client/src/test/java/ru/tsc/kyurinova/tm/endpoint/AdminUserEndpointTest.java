package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kyurinova.tm.marker.SoapCategory;

public class AdminUserEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private final UserEndpoint userEndpoint;

    @NotNull
    private SessionDTO session;

    @NotNull
    private final String userLogin = "login";

    @Nullable
    private String userId;

    @NotNull
    private UserDTO user;

    public AdminUserEndpointTest() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
    }

    @Before
    public void before() {
        session = sessionEndpoint.openSession("admin", "admin");
        user = adminUserEndpoint.createUserEmail(session, userLogin, "password", "login@mail.com");
        userId = user.getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(userEndpoint.findByIdUser(session, userId));
        adminUserEndpoint.removeByIdUser(session, userId);
        Assert.assertNull(userEndpoint.findByIdUser(session, userId));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByLoginTest() {
        Assert.assertNotNull(userLogin);
        Assert.assertNotNull(userEndpoint.findByLoginUser(session, userLogin));
        adminUserEndpoint.removeByLoginUser(session, userLogin);
        Assert.assertNull(userEndpoint.findByLoginUser(session, userLogin));
    }

    @Test
    @Category(SoapCategory.class)
    public void createUserTest() {
        @NotNull final String newUserLogin = "newUser";
        Assert.assertNotNull(adminUserEndpoint.createUser(
                session,
                newUserLogin,
                "newUser"
        ));
        Assert.assertNotNull(userEndpoint.findByLoginUser(session, newUserLogin));
        adminUserEndpoint.removeByLoginUser(session, newUserLogin);
    }

    @Test
    @Category(SoapCategory.class)
    public void createUserEmailTest() {
        @NotNull final String newUserLogin = "newUser";
        Assert.assertNotNull(adminUserEndpoint.createUserEmail(
                session,
                newUserLogin,
                "newUser",
                "newUser@mail.com"
        ));
        Assert.assertNotNull(userEndpoint.findByLoginUser(session, newUserLogin));
        adminUserEndpoint.removeByLoginUser(session, newUserLogin);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserTest() {
        @Nullable String userLastName = user.lastName;
        @Nullable String userFirstName = user.firstName;
        @Nullable String userMiddleName = user.middleName;
        adminUserEndpoint.updateUser(session, userId, "Иван", "Иванов", "Иванович");
        Assert.assertEquals("Иван", userEndpoint.findByIdUser(session, userId).firstName);
        Assert.assertEquals("Иванов", userEndpoint.findByIdUser(session, userId).lastName);
        Assert.assertEquals("Иванович", userEndpoint.findByIdUser(session, userId).middleName);
    }


    @Test
    @Category(SoapCategory.class)
    public void lockUnlockUserByLoginTest() {
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockUserByLogin(session, userLogin);
        Assert.assertTrue(userEndpoint.findByLoginUser(session, userLogin).isLocked());
        adminUserEndpoint.unlockUserByLogin(session, userLogin);
        Assert.assertFalse(userEndpoint.findByLoginUser(session, userLogin).isLocked());
    }

    @After
    public void after() {
        if (userId != null) adminUserEndpoint.removeByIdUser(session, userId);
        sessionEndpoint.closeSession(session);
    }

}
