package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.component.Bootstrap;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

public class SessionServiceTest {

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private SessionDTO session;

    @NotNull
    private String userId;

    @NotNull
    private final IUserService userService = new UserService(new ConnectionService(new PropertyService()), new LogService(), new PropertyService());


    public SessionServiceTest() {
        sessionService = new SessionService(
                new ConnectionService(new PropertyService()), new LogService(), new Bootstrap()
        );
    }

    @Before
    public void before() {
        @NotNull final String userLogin = "userLogin";
        @NotNull final String userPassword = "userPassword";
        userId = userService.create(userLogin, userPassword).getId();
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void openTest() {
        @NotNull final String newUserId = userService.create("test", "test").getId();
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO newSession = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(newSession.getSignature());
        sessionService.close(newSession);
        userService.removeById(newUserId);

    }

    @Test
    public void closeTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    public void validateTest() {
        @NotNull final String newUserId = userService.create("admin", "admin").getId();
        @NotNull final SessionDTO newSession = sessionService.open("admin", "admin");
        sessionService.validate(newSession);
        sessionService.close(newSession);
        userService.removeById(newUserId);
    }

    @Test
    public void validateRoleTest() {
        @NotNull final String newUserId = userService.create("admin", "admin", Role.ADMIN).getId();
        @NotNull final SessionDTO newSession = sessionService.open("admin", "admin");
        sessionService.validate(newSession, Role.ADMIN);
        sessionService.close(newSession);
        userService.removeById(newUserId);
    }

    @After
    public void after() {
        sessionService.close(session);
        userService.removeById(userId);
    }

}
