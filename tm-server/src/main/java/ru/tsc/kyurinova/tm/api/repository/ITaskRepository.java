package ru.tsc.kyurinova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO task (row_id, name, descr, user_id, status, created, start_dt, finish_dt, project_id)" +
            " VALUES (#{task.id}, #{task.name}, #{task.description}, #{task.userId}," +
            " #{task.status}, #{task.created}, #{task.startDate}, #{task.finishDate}, #{task.projectId})")
    void add(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("task") TaskDTO task
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId} AND name = #{name} LIMIT 1")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    TaskDTO findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE task SET status = 'IN_PROGRESS', start_dt = CURRENT_TIMESTAMP" +
            " WHERE row_id = #{id} AND user_id = #{userId} AND status <> 'IN_PROGRESS'")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE task SET status = 'IN_PROGRESS', start_dt = CURRENT_TIMESTAMP WHERE row_id in" +
            " (SELECT row_id from task where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Update("UPDATE task SET status = 'IN_PROGRESS', start_dt = CURRENT_TIMESTAMP" +
            " WHERE name = #{name} AND user_id = #{userId} AND status <> 'IN_PROGRESS'")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE task SET status = 'COMPLETED', finish_dt = CURRENT_TIMESTAMP" +
            " WHERE row_id = #{id} AND user_id = #{userId} AND status <> 'COMPLETED'")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE task SET status = 'COMPLETED', finish_dt = CURRENT_TIMESTAMP WHERE row_id in" +
            " (SELECT row_id from task where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Update("UPDATE task SET status = 'COMPLETED', finish_dt = CURRENT_TIMESTAMP" +
            " WHERE name = #{name} AND user_id = #{userId} AND status <> 'finish_dt'")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE task SET name = #{name}, descr = #{description}" +
            " WHERE user_id = #{userId} AND row_id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE task SET name = #{name}, descr = #{description} WHERE row_id in" +
            " (SELECT row_id from task where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE task SET status = #{status} WHERE user_id = #{userId} AND row_id = #{id}")
    void changeStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") Status status
    );

    @Update("UPDATE task SET status = #{status} WHERE row_id in" +
            " (SELECT row_id from task where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void changeStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") Status status
    );

    @Update("UPDATE task SET status = #{status} WHERE user_id = #{userId} AND name = #{name}")
    void changeStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") Status status
    );

    @Update("UPDATE task SET project_id = #{projectId} WHERE user_id = #{userId} AND row_id = #{taskId}")
    void bindTaskToProjectById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("taskId") String taskId
    );

    @Update("UPDATE task SET project_id = null WHERE project_id = #{projectId}" +
            " AND user_id = #{userId} AND row_id = #{taskId}")
    void unbindTaskToProjectById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("taskId") String taskId
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId} AND project_id = #{projectId} AND row_id = #{taskId}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @Nullable
    TaskDTO findByProjectAndTaskId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("taskId") String taskId
    );

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllTaskByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<TaskDTO> findAllTaskByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND row_id = #{task.id}")
    void remove(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("task") TaskDTO task
    );

    @Select("SELECT * FROM task")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<TaskDTO> findAll(
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<TaskDTO> findAllUserId(
            @NotNull @Param("userId") String userId
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId} ORDER BY #{comporator}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<TaskDTO> findAllComporator(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("comparator") String comparator
    );

    @Delete("DELETE FROM task WHERE user_id = #{userId}")
    void clearUserId(
            @NotNull @Param("userId") String userId
    );

    @Delete("DELETE FROM task")
    void clear(
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId} AND row_id = #{id}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @Nullable
    TaskDTO findById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    TaskDTO findByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND row_id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

}
