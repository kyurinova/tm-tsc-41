package ru.tsc.kyurinova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO project (row_id, name, descr, user_id, status, created, start_dt, finish_dt)" +
            " VALUES (#{project.id}, #{project.name}, #{project.description}, #{project.userId}," +
            " #{project.status}, #{project.created}, #{project.startDate}, #{project.finishDate})")
    void add(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("project") ProjectDTO project
    );

    @Select("SELECT * FROM project WHERE user_id = #{userId} AND name = #{name} LIMIT 1")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @NotNull
    ProjectDTO findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Delete("DELETE FROM project WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE project SET status = 'IN_PROGRESS', start_dt = CURRENT_TIMESTAMP" +
            " WHERE row_id = #{id} AND user_id = #{userId} AND status <> 'In Progress'")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );


    @Update("UPDATE project SET status = 'IN_PROGRESS', start_dt = CURRENT_TIMESTAMP WHERE row_id in" +
            " (SELECT row_id from project where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Update("UPDATE project SET status = 'IN_PROGRESS', start_dt = CURRENT_TIMESTAMP" +
            " WHERE name = #{name} AND user_id = #{userId} AND status <> 'In Progress'")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE project SET status = 'COMPLETED', finish_dt = CURRENT_TIMESTAMP" +
            " WHERE row_id = #{id} AND user_id = #{userId} AND status <> 'Completed'")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE project SET status = 'COMPLETED', finish_dt = CURRENT_TIMESTAMP WHERE row_id in" +
            " (SELECT row_id from project where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Update("UPDATE project SET status = 'COMPLETED', finish_dt = CURRENT_TIMESTAMP" +
            " WHERE name = #{name} AND user_id = #{userId} AND status <> 'Completed'")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE project SET name = #{name}, descr = #{description}" +
            " WHERE user_id = #{userId} AND row_id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE project SET name = #{name}, descr = #{description} WHERE row_id in" +
            " (SELECT row_id from project where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE project SET status = #{status} WHERE user_id = #{userId} AND row_id = #{id}")
    void changeStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") Status status
    );

    @Update("UPDATE project SET status = #{status} WHERE row_id in" +
            " (SELECT row_id from project where user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void changeStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") Status status
    );

    @Update("UPDATE project SET status = #{status} WHERE user_id = #{userId} AND name = #{name}")
    void changeStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") Status status
    );

    @Delete("DELETE FROM project WHERE user_id = #{userId} AND row_id = #{project.id}")
    void remove(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("project") ProjectDTO project
    );

    @Select("SELECT * FROM project")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @NotNull
    List<ProjectDTO> findAll(
    );

    @Select("SELECT * FROM project WHERE user_id = #{userId}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @NotNull
    List<ProjectDTO> findAllUserId(
            @NotNull @Param("userId") String userId
    );

    @Select("SELECT * FROM project WHERE user_id = #{userId} ORDER BY #{comporator}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @NotNull
    List<ProjectDTO> findAllComporator(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("comparator") String comparator
    );

    @Delete("DELETE FROM project")
    void clear(
    );

    @Delete("DELETE FROM project WHERE user_id = #{userId}")
    void clearUserId(
            @NotNull @Param("userId") String userId
    );

    @Select("SELECT * FROM project WHERE user_id = #{userId} AND row_id = #{id}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @Nullable
    ProjectDTO findById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "row_id", property = "id")
    @Result(column = "descr", property = "description")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_dt", property = "startDate")
    @Result(column = "finish_dt", property = "finishDate")
    @NotNull
    ProjectDTO findByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Delete("DELETE FROM project WHERE user_id = #{userId} AND row_id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

}