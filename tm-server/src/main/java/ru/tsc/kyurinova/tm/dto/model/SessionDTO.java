package ru.tsc.kyurinova.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "session")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Column(name = "time_stamp")
    private Long timestamp;

    @Column(name = "user_id")
    private String userId;

    private String signature;

    public SessionDTO() {
    }

}
