package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.entity.IWBS;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "project")
public class ProjectDTO extends AbstractOwnerEntityDTO implements IWBS {

    @NotNull
    public ProjectDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public ProjectDTO(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    public ProjectDTO(@NotNull String userId, @NotNull String name, @NotNull String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    public ProjectDTO(@NotNull String userId, @NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column(name = "descr")
    private String description = "";

    @NotNull
    @Column(name = "user_id")
    private String userId = "";

    @NotNull
    @Column
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_dt")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    private Date finishDate;

    @Nullable
    @Column
    private Date created = new Date();

}
