package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService {

    void addAll(@NotNull List<TaskDTO> tasks);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

    void updateById(@Nullable String userId, @Nullable String id, String name, @NotNull String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    void startById(@Nullable String userId, @Nullable String id);

    void startByIndex(@Nullable String userId, @Nullable Integer index);

    void startByName(@Nullable String userId, @Nullable String name);

    void finishById(@Nullable String userId, @Nullable String id);

    void finishByIndex(@Nullable String userId, @Nullable Integer index);

    void finishByName(@Nullable String userId, @Nullable String name);

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @Nullable
    TaskDTO findByProjectAndTaskId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void remove(@Nullable String userId, @Nullable TaskDTO task);

    @NotNull List<TaskDTO> findAll();

    @NotNull List<TaskDTO> findAll(@Nullable String userId);

    @NotNull List<TaskDTO> findAll(@Nullable String userId, @Nullable String comparator);

    void clear(@Nullable String userId);

    void clear();

    @Nullable TaskDTO findById(@Nullable String userId, @Nullable String id);

    @NotNull TaskDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsByIndex(@Nullable String userId, @NotNull Integer index);

}
