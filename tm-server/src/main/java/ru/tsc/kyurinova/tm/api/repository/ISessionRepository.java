package ru.tsc.kyurinova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import java.util.Comparator;
import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO session (row_id, user_id, signature, time_stamp)" +
            " VALUES (#{session.id}, #{session.userId}," +
            " #{session.signature}, #{session.timestamp})")
    void add(
            @NotNull @Param("session") SessionDTO session
    );

    @Select("SELECT * FROM session WHERE row_id = #{id}")
    @Result(column = "row_id", property = "id")
    @Result(column = "user_id", property = "userId")
    @Result(column = "time_stamp", property = "timestamp")
    @NotNull
    public SessionDTO findById(
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM session WHERE row_id = #{session.id}")
    void remove(
            @NotNull @Param("session") final SessionDTO session
    );

    @Select("SELECT * FROM session")
    @Result(column = "row_id", property = "id")
    @Result(column = "user_id", property = "userId")
    @Result(column = "time_stamp", property = "timestamp")
    @Nullable
    List<SessionDTO> findAll(
    );

    @Select("SELECT * FROM session ORDER BY #{comporator}")
    @Result(column = "row_id", property = "id")
    @Result(column = "user_id", property = "userId")
    @Result(column = "time_stamp", property = "timestamp")
    @Nullable
    List<SessionDTO> findAllComporator(
            @NotNull @Param("comparator") Comparator comparator
    );

    @Delete("DELETE FROM session")
    void clear(
    );

    @Select("SELECT * FROM session LIMIT 1 OFFSET #{index}")
    @Result(column = "row_id", property = "id")
    @Result(column = "user_id", property = "userId")
    @Result(column = "time_stamp", property = "timestamp")
    @Nullable
    @NotNull
    SessionDTO findByIndex(
            @NotNull @Param("index") final Integer index
    );

    @Delete("DELETE FROM session WHERE index = #{index}")
    void removeByIndex(
            @NotNull @Param("index") final Integer index
    );

    @Delete("DELETE FROM session WHERE row_id = #{id}")
    void removeById(
            @NotNull @Param("id") final String id
    );

    @Select("SELECT count(*) FROM session")
    int getSize(
    );

}
