package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import java.util.Comparator;
import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO open(@NotNull String login, @NotNull String password);

    @NotNull
    SessionDTO sign(@NotNull SessionDTO session);

    void close(@Nullable SessionDTO session);

    @NotNull
    boolean checkDataAccess(@NotNull String login, @NotNull String password);

    boolean exists(@NotNull String sessionId);

    void validate(@NotNull SessionDTO session);

    void validate(@NotNull SessionDTO session, @NotNull Role role);

    void remove(@Nullable SessionDTO entity);

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findAll(Comparator<SessionDTO> comparator);

    void clear();

    @Nullable
    SessionDTO findById(@Nullable String id);

    @NotNull
    SessionDTO findByIndex(@Nullable Integer index);

    void removeById(@Nullable String id);

    void removeByIndex(@Nullable Integer index);

    int getSize();
}
