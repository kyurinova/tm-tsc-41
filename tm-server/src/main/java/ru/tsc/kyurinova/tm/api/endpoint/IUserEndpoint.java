package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @WebMethod
    @NotNull List<UserDTO> findAllUser(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    @Nullable UserDTO findByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull UserDTO findByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    @Nullable UserDTO findByEmailUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    );

    @WebMethod
    void isLoginExistsUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );

    @WebMethod
    void isEmailExists(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    );

    @WebMethod
    @Nullable UserDTO findByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    );

}
