package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntityDTO implements Serializable {

    @NotNull
    @Column(name = "row_id")
    protected String Id = UUID.randomUUID().toString();

}
